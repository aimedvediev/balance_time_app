class Hobby {
  String id;
  String title;
  int time;
  double coefficient;

  Hobby({
    this.id,
    this.title,
    this.time,
    this.coefficient,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'title': title,
      'time': time,
      'coefficient': coefficient,
    };
    return map;
  }

  Hobby.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    title = map['title'];
    time = map['time'];
    coefficient = map['coefficient'];
  }
}
