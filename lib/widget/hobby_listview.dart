import 'package:flutter/material.dart';
import 'package:flutter_balance_app/providers/hobby_provider.dart';
import 'package:flutter_balance_app/widget/hobby_items.dart';
import 'package:provider/provider.dart';

import '../screen/hobby_screen.dart';

class HobbyListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<HobbyProvider>(context, listen: false)
          .fetchAndSetHobbies(),
      builder: (ctx, snapshot) => snapshot.connectionState ==
              ConnectionState.waiting
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Consumer<HobbyProvider>(
              child: Center(
                child: const Text('Got no places yet, start adding some!'),
              ),
              builder: (ctx, hobbyProvider, ch) => hobbyProvider.items.length <=
                      0
                  ? ch
                  : ListView.builder(
                      itemCount: hobbyProvider.items.length,
                      itemBuilder: (_, index) => FlatButton(
                        child: HobbyItems(
                          hobbyProvider.items[index].title,
                          hobbyProvider.items[index].time.toString(),
                          hobbyProvider.items[index].coefficient,
                        ),
                        onPressed: () {
                          Navigator.of(context).pushNamed(HobbyScreen.routeName,
                              arguments: hobbyProvider.items[index].id);
                        },
                      ),
                    ),
            ),
    );
  }
}
