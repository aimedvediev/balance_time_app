import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_balance_app/helpers/db_helper.data.dart';
import 'package:flutter_balance_app/model/hobby.dart';
import 'package:flutter_balance_app/providers/hobby_provider.dart';
import 'package:provider/provider.dart';

class PlayItem extends StatefulWidget {
  @override
  _PlayItemState createState() => _PlayItemState();
}

class _PlayItemState extends State<PlayItem> {
  bool isStart = false;
  bool checkTimer = true;
  String timerDisplay = '00:00:00';
  int timeForTimer;
  var watch = Stopwatch();
  final dur = const Duration(seconds: 1);

  bool isUpdating;
  var dbHelper;
  Hobby loadedHobby;

  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    isUpdating = false;
  }

  @override
  void didChangeDependencies() {
    loadedHobby =
        Provider.of<HobbyProvider>(context, listen: false).findByTitle('PLAY');
    timeForTimer = loadedHobby.time;
    timerDisplay = showTime(timeForTimer);
    super.didChangeDependencies();
  }

  void startTimer() {
    if (isStart) {
      isStart = false;
      Hobby hobby = Hobby(
        id: loadedHobby.id,
        time: timeForTimer,
        title: loadedHobby.title,
        coefficient: loadedHobby.coefficient,
      );
      DBHelper.updateTime(hobby);
    } else {
      setState(() {
        isStart = true;
      });
      Timer.periodic(dur, (Timer timer) {
        setState(() {
          if (timeForTimer < 1 || !isStart) {
            timer.cancel();
          } else {
            timeForTimer = timeForTimer - 1;
          }
          int h = timeForTimer ~/ 3600;
          int t = timeForTimer - (3600 * h);
          int m = t ~/ 60;
          int s = t - (60 * m);
          timerDisplay = h.toString().padLeft(2, '0') +
              ':' +
              m.toString().padLeft(2, '0') +
              ':' +
              s.toString().padLeft(2, '0');
        });
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.center,
            child: Text(
              timerDisplay,
              style: TextStyle(
                color: Color(0xFF2f2f2f),
                fontSize: 50,
              ),
            ),
          ),
        ),
        Container(
          height: 100,
          child: RaisedButton.icon(
            color: isStart ? Colors.yellow : Colors.blue,
            onPressed: startTimer,
            icon: !isStart ? Icon(Icons.play_arrow) : Icon(Icons.stop),
            label: !isStart ? Text('Start') : Text('Stop'),
          ),
        ),
      ],
    );
  }

  String showTime(timeForTimer) {
    int h = (timeForTimer) ~/ 3600;
    int t = (timeForTimer) - (3600 * h);
    int m = t ~/ 60;
    int s = t - (60 * m);
    return h.toString().padLeft(2, '0') +
        ':' +
        m.toString().padLeft(2, '0') +
        ':' +
        s.toString().padLeft(2, '0');
  }
}
