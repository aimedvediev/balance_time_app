import 'package:flutter/material.dart';

class HobbyItems extends StatelessWidget {
  final String name;
  final String time;
  final double coefficient;

  HobbyItems(
    this.name,
    this.time,
    this.coefficient,
  );

  String showTime(timeForTimer) {
    int h = int.parse(timeForTimer) ~/ 3600;
    int t = int.parse(timeForTimer) - (3600 * h);
    int m = t ~/ 60;
    int s = t - (60 * m);
    return h.toString().padLeft(2, '0') +
        ':' +
        m.toString().padLeft(2, '0') +
        ':' +
        s.toString().padLeft(2, '0');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
            showTime(time),
            style: TextStyle(
              color: Color(0xFF2f2f2f),
              fontSize: 30,
            ),
          ),
          Text(
            name,
            style: TextStyle(
              color: Color(0xFF1a1824),
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            '${coefficient.toStringAsFixed(2)}',
            style: TextStyle(
              color: Color(0xFF2f2f2f),
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
