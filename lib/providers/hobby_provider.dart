import 'package:flutter/cupertino.dart';
import 'package:flutter_balance_app/helpers/db_helper.data.dart';
import 'package:flutter_balance_app/model/hobby.dart';

class HobbyProvider with ChangeNotifier {
  List<Hobby> _items = [];

  List<Hobby> get items {
    return [..._items];
  }

  void addHobby(String title, double coefficient) {
    final newHobby = Hobby(
      id: DateTime.now().toString(),
      title: title,
      coefficient: coefficient,
      time: 0,
    );
    _items.add(newHobby);
    notifyListeners();
    DBHelper.insert('user_hobies', {
      'id': newHobby.id,
      'title': newHobby.title,
      'time': newHobby.time,
      'coefficient': newHobby.coefficient,
    });
  }

  Future<void> fetchAndSetHobbies() async {
    final dataList = await DBHelper.getData('user_hobies');
    _items = dataList
        .map(
          (item) => Hobby(
            id: item['id'],
            title: item['title'],
            time: item['time'],
            coefficient: item['coefficient'],
          ),
        )
        .toList();
    notifyListeners();
  }

  findById(String hobbyId) {
    return _items.firstWhere((hobby) => hobby.id == hobbyId);
  }

  findByTitle(String title) {
    return _items.firstWhere((hobby) => hobby.title == title);
  }
}
