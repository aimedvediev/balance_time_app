import 'package:flutter/material.dart';
import 'package:flutter_balance_app/providers/hobby_provider.dart';
import 'package:provider/provider.dart';

class AddHobbyScreen extends StatefulWidget {
  static const routeName = '/add-hobby';

  @override
  _AddHobbyScreenState createState() => _AddHobbyScreenState();
}

class _AddHobbyScreenState extends State<AddHobbyScreen> {
  final _titleController = TextEditingController();
  double coefficient = 0.5;

  void _savePlace() {
    if (_titleController.toString().length < 3) {
      return;
    }
    Provider.of<HobbyProvider>(context,listen: false)
        .addHobby(_titleController.text, coefficient);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a new hobby'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(labelText: 'Title'),
                      controller: _titleController,
                    ),
                    Row(
                      children: <Widget>[
                        FlatButton(
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 50,
                          ),
                          onPressed: () {
                            setState(() {
                              if (coefficient > 0.06) coefficient -= 0.05;
                            });
                          },
                        ),
                        Text(
                          '${coefficient.toStringAsFixed(2)}',
                          style: TextStyle(fontSize: 50),
                        ),
                        FlatButton(
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 50,
                          ),
                          onPressed: () {
                            setState(() {
                              if (coefficient < 1) coefficient += 0.05;
                            });
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 100,
            child: RaisedButton.icon(
              onPressed: _savePlace,
              icon: Icon(Icons.add),
              label: Text('Add Hobby'),
            ),
          ),
        ],
      ),
    );
  }
}
