import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_balance_app/helpers/db_helper.data.dart';
import 'package:flutter_balance_app/model/hobby.dart';
import 'package:flutter_balance_app/providers/hobby_provider.dart';
import 'package:provider/provider.dart';

class HobbyScreen extends StatefulWidget {
  static const routeName = '/hobby-screen';

  @override
  _HobbyScreenState createState() => _HobbyScreenState();
}

class _HobbyScreenState extends State<HobbyScreen> {
  bool isStart = false;
  String timerDisplay = '00:00:00';
  var watch = Stopwatch();
  final dur = const Duration(seconds: 1);
  var hobbyId;

  bool isUpdating;
  var dbHelper;
  Hobby loadedHobby;
  Hobby loadedPlay;

  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    isUpdating = false;
  }

  @override
  void didChangeDependencies() {
    hobbyId = ModalRoute.of(context).settings.arguments as String;
    loadedHobby =
        Provider.of<HobbyProvider>(context, listen: false).findById(hobbyId);
    super.didChangeDependencies();
    loadedPlay =
        Provider.of<HobbyProvider>(context, listen: false).findByTitle('PLAY');
  }

  void startTimer() {
    Timer(dur, keepRunning);
  }

  void keepRunning() {
    if (watch.isRunning) {
      startTimer();
    }
    setState(() {
      timerDisplay = watch.elapsed.inHours.toString().padLeft(2, '0') +
          ':' +
          (watch.elapsed.inMinutes % 60).toString().padLeft(2, '0') +
          ':' +
          (watch.elapsed.inSeconds % 60).toString().padLeft(2, '0');
    });
  }

  void startStopWatch() {
    if (isStart) {
      setState(() {
        isStart = false;
      });
      watch.stop();
      Hobby hobby = Hobby(
        id: hobbyId,
        time: loadedHobby.time +
            (watch.elapsed.inSeconds * loadedHobby.coefficient).round(),
        title: loadedHobby.title,
        coefficient: loadedHobby.coefficient,
      );
      Hobby hobbyPlay = Hobby(
        id: loadedPlay.id,
        time: loadedPlay.time +
            (watch.elapsed.inSeconds * loadedHobby.coefficient).round(),
        title: loadedPlay.title,
        coefficient: loadedPlay.coefficient,
      );
      DBHelper.updateTime(hobby);
      DBHelper.updateTime(hobbyPlay);
    } else {
      setState(() {
        isStart = true;
      });
      watch.start();
      startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Container(
              alignment: Alignment.center,
              child: Text(
                timerDisplay,
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Container(
            height: 100,
            child: RaisedButton.icon(
              color: isStart ? Colors.red : Colors.green,
              onPressed: startStopWatch,
              icon: !isStart ? Icon(Icons.play_arrow) : Icon(Icons.stop),
              label: !isStart ? Text('Start') : Text('Stop'),
            ),
          ),
        ],
      ),
    );
  }
}
