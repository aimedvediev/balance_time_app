import 'package:flutter/material.dart';
import 'package:flutter_balance_app/screen/add_hobby_screen.dart';
import 'package:flutter_balance_app/widget/hobby_listview.dart';
import 'package:flutter_balance_app/widget/play_widget.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.grey,
            onPressed: () {
              Navigator.of(context).pushNamed(AddHobbyScreen.routeName);
            },
          ),
          backgroundColor: Color(0xFFf4f1ea),
          appBar: AppBar(
            bottom: TabBar(
              tabs: <Widget>[
                Tab(text: 'Hobby'),
                Tab(text: 'Play'),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              Container(
                child: HobbyListView(),
              ),
              Container(
                child: PlayItem(),
              ),
            ],
          ),
        ),
    );
  }
}
