import 'package:flutter/material.dart';
import 'package:flutter_balance_app/providers/hobby_provider.dart';
import 'package:flutter_balance_app/screen/add_hobby_screen.dart';
import 'package:flutter_balance_app/screen/home_screen.dart';
import 'package:flutter_balance_app/screen/hobby_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: HobbyProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomeScreen(),
        routes: {
          HobbyScreen.routeName: (ctx) => HobbyScreen(),
          AddHobbyScreen.routeName: (ctx) => AddHobbyScreen(),
        },
      ),
    );
  }
}
