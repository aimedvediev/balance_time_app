import 'package:flutter_balance_app/model/hobby.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class DBHelper {
  static Future<sql.Database> dataBase() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'hob.db'),
        onCreate: (db, version) {
      db.execute('''CREATE TABLE user_hobies(id TEXT PRIMARY KEY, title TEXT, time INTEGER, coefficient DOUBLE)''');
      print("Database was created!");
    }, version: 1);
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DBHelper.dataBase();
    db.insert(
      table,
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<int> updateTime(Hobby hobby) async {
    var db = await DBHelper.dataBase();
    print(hobby.title+' updated');
    return await db.update('user_hobies', hobby.toMap(),
        where: 'id = ?', whereArgs: [hobby.id]);
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DBHelper.dataBase();
    return db.query(table);
  }
}
